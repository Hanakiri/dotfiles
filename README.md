# Hanakiri's dotfiles

This is my dotfiles repository for my Arch Linux system.
I also daily driver NixOS, and those dotfiles can be found at: https://gitlab.com/Hanakiri/dotfiles/-/tree/nixos?ref_type=heads
